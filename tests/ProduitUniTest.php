<?php

namespace App\Tests;
use App\Entity\Categorie;
use App\Entity\Produit;
use App\Entity\User;
use DateTime;

use PHPUnit\Framework\TestCase;

class ProduitUnitTest extends TestCase
{
    public function testIsTrue()
    {
      $produit = new Produit();
      $date=new DateTime();
      $categorie=new Categorie();
      $user= new User();

      $produit-> setNomProduit('nomProduit')
              -> setPrix(20.20)
              -> setDescription('description')
              -> setImage('image')
              -> setDate($date)
              -> setQuantite(4)
              -> setSlug('slug')
              -> addCategorie($categorie)
              -> setUser($user);

      $this->assertTrue($produit->getNomProduit()==='nomProduit');
      $this->assertTrue($produit->getPrix()===20.20);
      $this->assertTrue($produit->getDescription()==='description');
      $this->assertTrue($produit->getImage()==='image');
      $this->assertTrue($produit->getDate()===$date);
      $this->assertTrue($produit->getQuantite()=== 4 );
      $this->assertTrue($produit->getSlug()==='slug'); 
      $this->assertContains($categorie, $produit ->getCategorie());
      $this->assertTrue($produit->getUser()===$user);       
       // $this->assertTrue(true);
    }
    public function testIsFalse()
    {
        $produit = new Produit();
        $date=new DateTime();
        $categorie=new Categorie();
        $user= new User();

      $produit-> setNomProduit('nomProduit')
              -> setPrix(20.20)
              -> setDescription('description')
              -> setImage('image')
              -> setDate($date)
              -> setQuantite(4)
              -> setSlug('slug')
              -> addCategorie($categorie)
              -> setUser($user);
           
      $this->assertFalse($produit->getNomProduit()==='false');
      $this->assertFalse($produit->getPrix()===22.00);
      $this->assertFalse($produit->getDescription()==='false');
      $this->assertFalse($produit->getImage()=== 'false');
      $this->assertFalse($produit->getDate()===new DateTime());
      $this->assertFalse($produit->getQuantite()=== 5 );
      $this->assertFalse($produit->getSlug()==='false');    
      $this->assertNotContains(new Categorie(), $produit ->getCategorie());
      $this->assertFalse($produit->getUser()===new User());    
       // $this->assertTrue(true);
    }
    public function testIsEmpty()
    {
      $produit = new Produit();    
           
      $this->assertEmpty($produit->getNomProduit());
      $this->assertEmpty($produit->getPrix());
      $this->assertEmpty($produit->getDescription());
      $this->assertEmpty($produit->getImage());
      $this->assertEmpty($produit->getDate());
      $this->assertEmpty($produit->getQuantite());
      $this->assertEmpty($produit->getSlug()); 
      $this->assertEmpty($produit->getCategorie());
      $this->assertEmpty($produit->getUser());           
      
    }
}
