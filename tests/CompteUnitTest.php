<?php

namespace App\Tests;

use App\Entity\Compte;
use App\Entity\User;
use DateTimeImmutable;
use PHPUnit\Framework\TestCase;

class CompteUnitTest extends TestCase
{
    public function testIsTrue()
    {
      $compte = new Compte();
      $datetime = new DateTimeImmutable();

      $compte   -> setCreatedAt($datetime)
                -> setEmail('email')
                -> setPassword('password');           
           
      $this->assertTrue($compte->getCreatedAt()===$datetime);
      $this->assertTrue($compte->getEmail()==='email');
      $this->assertTrue($compte->getPassword()==='password');
      
       // $this->assertTrue(true);
    }
    public function testIsFalse()
    {
      $compte = new Compte();
      $datetime = new DateTimeImmutable();

      $compte   -> setCreatedAt($datetime)
                -> setEmail('email')
                -> setPassword('password');
           
      
      $this->assertFalse($compte->getCreatedAt()=== new $datetime());
      $this->assertFalse($compte->getEmail()==='false');
      $this->assertFalse($compte->getPassword()==='false');
      
       // $this->assertTrue(true);
    }
    public function testIsEmpty()
    {
      $compte = new Compte();    
  
      $this->assertEmpty($compte->getCreatedAt());
      $this->assertEmpty($compte->getEmail());
      $this->assertEmpty($compte->getPassword());

      
    }
}
