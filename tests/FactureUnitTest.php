<?php

namespace App\Tests;

use App\Entity\Facture;
use App\Entity\User;
use DateTimeImmutable;
use PHPUnit\Framework\TestCase;

class FactureUnitTest extends TestCase
{
    function testIsTrue()
    {
      $facture = new Facture();
      $datetime = new DateTimeImmutable();

      $facture -> setCreatedAt($datetime);
                        
           
      $this->assertTrue($facture->getCreatedAt()===$datetime);
     
      
       // $this->assertTrue(true);
    }
    public function testIsFalse()
    {
      $facture = new Facture();
      $datetime = new DateTimeImmutable();

      $facture -> setCreatedAt($datetime);               
           
      
      $this->assertFalse($facture->getCreatedAt()=== new $datetime());
      
       // $this->assertTrue(true);
    }
    public function testIsEmpty()
    {
      $facture = new Facture();    
  
      $this->assertEmpty($facture->getCreatedAt());
      
      
    }
}
