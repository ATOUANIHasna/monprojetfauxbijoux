<?php

namespace App\Tests;

use App\Entity\Reglement;
use DateTimeImmutable;
use PHPUnit\Framework\TestCase;

class ReglementUnitTest extends TestCase
{
    public function testIsTrue()
    {
      $reglement = new Reglement();
      $datetime = new DateTimeImmutable();

      $reglement-> setCreatedAt($datetime)
                -> setMontant(4.3);           
           
      $this->assertTrue($reglement->getCreatedAt()===$datetime);
      $this->assertTrue($reglement->getMontant()=== 4.3);
      
       // $this->assertTrue(true);
    }
    public function testIsFalse()
    {
      $reglement = new Reglement();
      $datetime = new DateTimeImmutable();

      $reglement-> setCreatedAt($datetime)
                -> setMontant(4.3);           
      
      $this->assertFalse($reglement->getCreatedAt()=== new $datetime());
      $this->assertFalse($reglement->getMontant()===4.2);
      
       // $this->assertTrue(true);
    }
    public function testIsEmpty()
    {
      $reglement = new Reglement();  
                                                       
       $this->assertEmpty($reglement->getCreatedAt());
       $this->assertEmpty($reglement->getMontant());

    }
}
