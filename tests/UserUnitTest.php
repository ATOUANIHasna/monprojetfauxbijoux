<?php

namespace App\Tests;

use App\Entity\User;
use PHPUnit\Framework\TestCase;

class UserUnitTest extends TestCase
{
    public function testIsTrue()
    {
      $user = new User();

      $user-> setEmail('true@testcom')
           -> setPassword('password')
           -> setNom('nom')
           -> setPrenom('prenom')
           -> setSexe('sexe')
           -> setNumeroTelephone('numeroTelephone')           
           -> setAdresse('adresse');
           
      $this->assertTrue($user->getEmail()==='true@testcom');
      $this->assertTrue($user->getPassword()==='password');
      $this->assertTrue($user->getNom()==='nom');
      $this->assertTrue($user->getPrenom()==='prenom');
      $this->assertTrue($user->getSexe()==='sexe');
      $this->assertTrue($user->getNumeroTelephone()==='numeroTelephone');      
      $this->assertTrue($user->getAdresse()==='adresse');       
       // $this->assertTrue(true);
    }
    public function testIsFalse()
    {
      $user = new User();

      $user-> setEmail('true@testcom')
           -> setPassword('password')
           -> setNom('nom')
           -> setPrenom('prenom')
           -> setSexe('sexe')
           -> setNumeroTelephone('numeroTelephone')           
           -> setAdresse('adresse');
           
      $this->assertFalse($user->getEmail()==='false@testcom');
      $this->assertFalse($user->getPassword()==='false');
      $this->assertFalse($user->getNom()==='false');
      $this->assertFalse($user->getPrenom()==='false');
      $this->assertFalse($user->getSexe()==='false');
      $this->assertFalse($user->getNumeroTelephone()==='false');     
      $this->assertFalse($user->getAdresse()==='false');       
       // $this->assertTrue(true);
    }
    public function testIsEmpty()
    {
      $user = new User();    
           
      $this->assertEmpty($user->getEmail());
      $this->assertEmpty($user->getPassword());
      $this->assertEmpty($user->getNom());
      $this->assertEmpty($user->getPrenom());
      $this->assertEmpty($user->getSexe());
      $this->assertEmpty($user->getNumeroTelephone());      
      $this->assertEmpty($user->getAdresse());       
      
    }
}
