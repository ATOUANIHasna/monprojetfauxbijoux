<?php

namespace App\Controller;

use App\Repository\CategorieRepository;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;
use App\Repository\ProduitRepository;

class CollierController extends AbstractController
{
    /**
     * @Route("/collier", name="collier")
     */
    
    public function index(ProduitRepository $produitRepository): Response
    {
        return $this->render('collier/index.html.twig', [
            'produits' => $produitRepository->findAll()
        ]);
        
    }
    
}
