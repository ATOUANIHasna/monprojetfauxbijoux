<?php

namespace App\Controller;

use App\Repository\CategorieRepository;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;
use App\Repository\ProduitRepository;

class BoucledoreillesController extends AbstractController
{
    /**
     * @Route("/boucledoreilles", name="boucledoreilles")
     */
    
    public function index(ProduitRepository $produitRepository): Response
    {
        return $this->render('boucledoreilles/index.html.twig', [
            'produits' => $produitRepository->findAll()
        ]);
        
    }
    
}
