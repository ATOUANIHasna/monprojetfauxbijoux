<?php

namespace App\Controller;
use App\Entity\Produit;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\HttpFoundation\Session\SessionInterface;
use Symfony\Component\Routing\Annotation\Route;
use App\Repository\ProduitRepository;

    /**
     * @Route("/cart", name="cart_")
     */
class CartController extends AbstractController
{
    /**
     * @Route("/", name="index")
     */
    public function index(SessionInterface $session, ProduitRepository $produitrepository)
    {

        $panier = $session->get("panier", []);
        // On "fabrique" les données
        $dataPanier=[];
        $total = 0;

        foreach($panier as $id => $quantite){

            $produit = $produitrepository->find($id);
            $dataPanier[] = [

                "produit" =>$produit,
                "quantite" => $quantite
            ];
           $total += $produit->getPrix() * $quantite;
        }
        return $this->render('cart/index.html.twig', compact ("dataPanier","total"));
    }
    /**
     * @Route("/add/{id}", name="add")
     */
    public function add(Produit $produit, SessionInterface $session)
    {
       // on récupère le panier actuel
       
       $panier = $session->get("panier", []);
       $id = $produit->getId();

       if(!empty($panier[$id])){
           $panier[$id]++;

       }
       else{  

           $panier[$id]= 1;

       }
    
       //on sauvegarde dans la session
    
        $session->set("panier", $panier);
    
      // dd($session);
        return $this->redirectToRoute('cart_index');
    }

    /**
     * @Route("/remove/{id}", name="remove")
     */
    public function remove(Produit $produit, SessionInterface $session)
    {
       // on récupère le panier actuel
       
       $panier = $session->get("panier", []);
       $id = $produit->getId();

       if(!empty($panier[$id])){

           if($panier[$id] > 1) {
               $panier[$id]--;
           }else{
               unset($panier[$id]);
           }       

       } else{  

           $panier[$id]= 1;

       }
    
       //on sauvegarde dans la session
    
        $session->set("panier", $panier);
    
      // dd($session);
        return $this->redirectToRoute('cart_index');
    }
    /**
     * @Route("/delete/{id}", name="delete")
     */
    public function delete(Produit $produit, SessionInterface $session)
    {
       // on récupère le panier actuel
       
       $panier = $session->get("panier", []);
       $id = $produit->getId();

       if(!empty($panier[$id])){           
        unset($panier[$id]);
       }  
    
       //on sauvegarde dans la session
    
        $session->set("panier", $panier);
    
      // dd($session);
        return $this->redirectToRoute('cart_index');
    }
    /**
     * @Route("/delete_all", name="delete_all")
     */
    public function deleteALL(SessionInterface $session)
    {
       // on récupère le panier actuel
       
       $panier = $session->get("panier", []);
               
        unset($panier);
       
    
       //on sauvegarde dans la session
    
        $session->set("panier", []);
    
      // dd($session);
        return $this->redirectToRoute('cart_index');
    }
    
}
