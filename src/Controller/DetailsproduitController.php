<?php

namespace App\Controller;
use App\Entity\Produit;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;

class DetailsproduitController extends AbstractController
{
    /**

     *  @Route("/produit/detailsproduit/{id}", name="detailsproduit")
     */
    public function index(Produit $produit): Response
    {
        
        return $this->render('detailsproduit/index.html.twig', [
            'controller_name' => 'DetailsproduitController',
            'produit' => $produit,
        ]);
    }



}
