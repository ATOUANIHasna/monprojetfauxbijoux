<?php

namespace App\Controller;

use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;

class MonpanierController extends AbstractController
{
    /**
     * @Route("/monpanier", name="monpanier")
     */
    public function index(): Response
    {
        return $this->render('monpanier/index.html.twig', [
            'controller_name' => 'MonpanierController',
        ]);
    }
}
