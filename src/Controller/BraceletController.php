<?php

namespace App\Controller;

use App\Repository\CategorieRepository;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;
use App\Repository\ProduitRepository;

class BraceletController extends AbstractController
{
    /**
     * @Route("/bracelet", name="bracelet")
     */
    
    public function index(ProduitRepository $produitRepository): Response
    {
        return $this->render('bracelet/index.html.twig', [
            'produits' => $produitRepository->findAll()
        ]);
        
    }
    
}
