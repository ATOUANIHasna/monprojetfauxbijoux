<?php

namespace App\Controller;

use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;

class ValidermonpanierController extends AbstractController
{
    /**
     * @Route("/validermonpanier", name="validermonpanier")
     */
    public function index(): Response
    {
        return $this->render('validermonpanier/index.html.twig', [
            'controller_name' => 'ValidermonpanierController',
        ]);
    }
}
