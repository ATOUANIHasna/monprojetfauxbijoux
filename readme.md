# FAUXBIJOUX

FAUXBIJOUX est un site internet présentant des faux bijoux traditionnels

### Environnement de développement 

### Pré-requis

* PHP 74
* Composer
* Symfony CLI
* Docker
* Docker-compose

Vous pouvez vérifier les pré-requis (sauf Docker et Docker-compose) avec la commande suivante(de la CLI Symfony):

````bash
symfony check:requirements
````

### Lancer l' environnement  de développemnt

```bash
docker-compse uo -d
symfony serve -d
```

#### Lancer des tests
```bash
php bin/phpunit --testdox
```
